# frozen_string_literal: true

# Job to run DisbursementService for a given merchant_id
class MerchantDisbursementJob < ApplicationJob
  queue_as :default

  def perform(merchant_id)
    DisbursementService.call(Merchant.find(merchant_id))
  end
end
