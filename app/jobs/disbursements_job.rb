# frozen_string_literal: true

# Job to enqueue all disbursement jobs for all merchants
class DisbursementsJob < ApplicationJob
  queue_as :default

  def perform
    Merchant.find_each do |merchant|
      MerchantDisbursementJob.perform_later(merchant.id)
    end
  end
end
