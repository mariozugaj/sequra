# frozen_string_literal: true

# Query for disbursements
class DisbursementsQuery
  attr_reader :search_params

  def initialize(search_params)
    @search_params = search_params
  end

  def result
    if merchant
      Disbursement.where(created_at: date_from..date_to, merchant: merchant)
    else
      Disbursement.where(created_at: date_from..date_to)
    end
  end

  private

  def merchant
    search_params[:merchant]
  end

  def date_from
    Date.parse(search_params[:date][:from]).beginning_of_day
  end

  def date_to
    Date.parse(search_params[:date][:to]).end_of_day
  end
end
