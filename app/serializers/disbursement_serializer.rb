# frozen_string_literal: true

# Disbursements serializer
class DisbursementSerializer
  include FastJsonapi::ObjectSerializer

  attribute :fee do |object|
    object.fee.to_d
  end
  attribute :amount do |object|
    object.amount.to_d
  end
  attribute :merchant_id
  attribute :created_at

  belongs_to :merchant

  cache_options enabled: true
end
