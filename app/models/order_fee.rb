# frozen_string_literal: true

# Calculates order fee amount and rate based on order amount
class OrderFee
  include ActiveModel::Validations

  attr_reader :input_amount, :fee_intervals

  validates :input_amount, numericality: { greater_than_or_equal_to: 0 }
  validate :input_amount_is_money

  def initialize(input_amount, fee_intervals)
    @input_amount = input_amount
    @fee_intervals = fee_intervals
    validate!
  end

  def amount
    input_amount * rate
  end

  def rate
    fee_intervals.find do |fee_interval|
      input_amount.cents.in?(fee_interval.from..fee_interval.to)
    end.fee_rate
  end

  private

  def input_amount_is_money
    return if input_amount.is_a? Money

    errors.add(:input_amount, 'must be a Money object')
  end
end
