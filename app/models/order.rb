# frozen_string_literal: true

class Order < ApplicationRecord
  belongs_to :merchant
  belongs_to :shopper
  belongs_to :disbursement, optional: true

  monetize :amount_cents, numericality: { greater_than_or_equal_to: 0 }

  enum status: { created: 0, completed: 1, disbursed: 2 }
end
