# frozen_string_literal: true

class FeeInterval < ApplicationRecord
  belongs_to :merchant

  validates :from, numericality: { greater_than_or_equal_to: 0 }
  validates :to, numericality: { greater_than: 0 }, allow_nil: true
  validates :fee_rate,
            numericality: { greater_than_or_equal_to: 0.0, less_than: 100.0 }
end
