# frozen_string_literal: true

class Merchant < ApplicationRecord
  has_many :disbursements
  has_many :orders
  has_many :fee_intervals
end
