# frozen_string_literal: true

class Disbursement < ApplicationRecord
  belongs_to :merchant
  has_many :orders

  monetize :fee_cents, numericality: { greater_than_or_equal_to: 0 }
  monetize :amount_cents, numericality: { greater_than_or_equal_to: 0 }
end
