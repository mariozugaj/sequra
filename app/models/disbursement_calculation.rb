# frozen_string_literal: true

# Calculates disbursement amount, fee and sum of completed orders amounts for a
# given merchant
class DisbursementCalculation
  attr_reader :merchant

  def initialize(merchant)
    @merchant = merchant
  end

  def fee
    @fee ||= completed_orders.inject(Money.new(0)) do |sum, order|
      sum + OrderFee.new(order.amount, @merchant.fee_intervals).amount
    end
  end

  def amount
    @amount ||= orders_amount_sum - fee
  end

  def orders_amount_sum
    @orders_amount_sum ||= Money.new(completed_orders.sum('amount_cents'))
  end

  private

  def completed_orders
    @completed_orders ||= merchant.orders.completed
  end
end
