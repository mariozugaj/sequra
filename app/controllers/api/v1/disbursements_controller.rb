# frozen_string_literal: true

module API
  module V1
    # Disbursements controller
    class DisbursementsController < API::V1::BaseController
      # GET /api/v1/disbursements
      def index
        if search_params.failure?
          json_response(search_params.errors(full: true).to_h, 400)
        else
          @pagy, disbursements =
            pagy(DisbursementsQuery.new(search_params).result)
          json_response(
            DisbursementSerializer.new(disbursements).serialized_json
          )
        end
      end

      private

      def params_schema
        Dry::Schema.Params do
          required(:date).hash do
            required(:from).filter(format?: /\d{4}-\d{2}-\d{2}/)
            required(:to).filter(format?: /\d{4}-\d{2}-\d{2}/)
          end
          optional(:merchant).filled(:integer)
        end
      end
    end
  end
end
