# frozen_string_literal: true

module API
  module V1
    # Base API controller
    class BaseController < ApplicationController
      include TokenAuthenticable
      include Pagy::Backend

      after_action { pagy_headers_merge(@pagy) if @pagy }

      private

      def search_params
        params_schema.call(params.permit!.to_h)
      end
    end
  end
end
