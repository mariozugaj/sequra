# frozen_string_literal: true

# Basic Bearer Token authentication
module TokenAuthenticable
  include ActionController::HttpAuthentication::Token::ControllerMethods
  extend ActiveSupport::Concern

  TOKEN = Rails.application.credentials.api_token

  included { before_action :authenticate }

  private

  def authenticate
    authenticate_or_request_with_http_token do |token, _options|
      ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)
    end
  end
end
