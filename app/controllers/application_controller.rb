# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  def not_found
    raise ActionController::RoutingError,
          "No route matches #{params[:unmatched_route]}"
  end
end
