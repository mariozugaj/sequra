# frozen_string_literal: true

# Service to calculate disbursement for a given merchant and its completed
# orders, create the disbursement and disburse all completed orders
class DisbursementService
  def self.call(merchant)
    new(merchant).call
  end

  def call
    return if calculation.fee.zero? && calculation.amount.zero?

    disburse_merchant_orders
    disbursement
  end

  private

  def initialize(merchant)
    @merchant = merchant
  end

  def calculation
    @calculation ||= DisbursementCalculation.new(merchant)
  end

  def disburse_merchant_orders
    merchant.orders.completed.update_all(
      status: 'disbursed',
      disbursement_id: disbursement.id
    )
  end

  def disbursement
    @disbursement ||=
      Disbursement.create!(
        merchant: merchant,
        fee: calculation.fee,
        amount: calculation.amount
      )
  end

  attr_reader :merchant
end
