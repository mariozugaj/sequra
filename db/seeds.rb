# frozen_string_literal: true

require 'csv'

Merchant.delete_all
Shopper.delete_all
Order.delete_all

CSV.foreach('data/merchants.csv', headers: false) do |row|
  Merchant.create!(id: row[0], name: row[1], email: row[2], cif: row[3])
end

CSV.foreach('data/shoppers.csv', headers: false) do |row|
  Shopper.create!(id: row[0], name: row[1], email: row[2], nif: row[3])
end

CSV.foreach('data/orders.csv', headers: false) do |row|
  Order.create!(
    id: row[0],
    merchant_id: row[1],
    shopper_id: row[2],
    amount_cents: Money.from_amount(row[3].to_d).cents,
    status: row[5].nil? ? 0 : 1,
    created_at: Date.parse(row[4]),
    completed_at: row[5].nil? ? nil : Date.parse(row[5])
  )
end
