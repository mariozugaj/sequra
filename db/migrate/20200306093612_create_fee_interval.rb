class CreateFeeInterval < ActiveRecord::Migration[6.0]
  def change
    create_table :fee_intervals do |t|
      t.references :merchant
      t.integer :from
      t.integer :to
      t.decimal :fee_rate
    end
  end
end
