# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.references :merchant, null: false, foreign_key: true
      t.references :shopper, null: false, foreign_key: true
      t.references :disbursement, foreign_key: true
      t.monetize :amount
      t.integer :status, null: false, default: 0
      t.datetime :completed_at

      t.timestamps
    end
  end
end
