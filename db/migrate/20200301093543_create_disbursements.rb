# frozen_string_literal: true

class CreateDisbursements < ActiveRecord::Migration[6.0]
  def change
    create_table :disbursements do |t|
      t.references :merchant, null: false, foreign_key: true
      t.monetize :fee
      t.monetize :amount

      t.timestamps
    end
  end
end
