# Sequra test challenge

This is a simple Ruby on Rails API application that calculates per merchant
disbursements and runs them every Monday at 12pm. It also exposes an API
endpoint to retrieve persisted disbursements on a given date and for a given
merchant.


## Requirements
- Ruby 2.7.0
- Rails > 6
- Master key to decrypt credentials

Check your versions by running

```shell
ruby -v
rails -v
```

## Installing / Getting started

To start the application, run following commands:

```shell
git clone https://gitlab.com/mariozugaj/sequra.git
cd sequra/
bundle install
bin/rails db:setup
bin/rails server
```

This will do the following:
- clone the repository,
- install all necessary dependencies listed in Gemfile,
- create and seed the database (from `data` files, it might take some time),
- start the Rails server.

You can access the application at http://localhost:3000

## Using
Head over to API documentation ([link bellow ](#links)) and see examples there.

## Testing

Run tests by executing following:

```shell
bin/rails test
```
## Links

- [Project homepage](https://gitlab.com/mariozugaj/sequra.git)
- [Repository](https://gitlab.com/mariozugaj/sequra.git)
- [API documentation](https://documenter.getpostman.com/view/8855082/SzKZsbxh?version=latest)

## Licensing

The code in this project is licensed under MIT license.
