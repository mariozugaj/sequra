# frozen_string_literal: true

require 'test_helper'

class OrderFeeTest < ActiveSupport::TestCase
  setup do
    @merchant = create :merchant
  end

  test 'raises argument error when input amount negative' do
    error =
      assert_raises(ActiveModel::ValidationError) do
        OrderFee.new(Money.new(-100), [])
      end

    assert_equal(
      'Validation failed: Input amount must be greater than or equal to 0',
      error.message
    )
  end

  test 'raises argument error when input not a Money object' do
    error =
      assert_raises(ActiveModel::ValidationError) { OrderFee.new(100, []) }

    assert_equal 'Validation failed: Input amount must be a Money object',
                 error.message
  end

  test 'fee rate when input amount <= 4999' do
    order_fee = OrderFee.new(Money.new(2_500), @merchant.fee_intervals)

    assert_equal 0.01.to_d, order_fee.rate
  end

  test 'fee rate when 50_00 < input amount <= 299_99 ' do
    order_fee = OrderFee.new(Money.new(175_00), @merchant.fee_intervals)

    assert_equal 0.0095.to_d, order_fee.rate
  end

  test 'fee rate when input amount >= 300_00 ' do
    order_fee = OrderFee.new(Money.new(370_00), @merchant.fee_intervals)

    assert_equal 0.0085.to_d, order_fee.rate
  end

  test 'fee amount when input amount <= 49_99' do
    order_fee = OrderFee.new(Money.new(25_00), @merchant.fee_intervals)

    assert_equal 25, order_fee.amount.cents
  end

  test 'fee amount when 50_00 < input amount <= 299_99 ' do
    order_fee = OrderFee.new(Money.new(175_00), @merchant.fee_intervals)

    assert_equal 166, order_fee.amount.cents
  end

  test 'fee amount when input amount >= 300_00 ' do
    order_fee = OrderFee.new(Money.new(370_00), @merchant.fee_intervals)

    assert_equal 315, order_fee.amount.cents
  end
end
