# frozen_string_literal: true

require 'test_helper'

class DisbursementCalculationTest < ActiveSupport::TestCase
  setup do
    @merchant = create(:merchant)
    @another_merchant = create(:merchant)
  end

  test '#fee' do
    create_completed_orders(@merchant)

    assert_equal 30, DisbursementCalculation.new(@merchant).fee.cents
  end

  test '#amount' do
    create_completed_orders(@merchant)

    assert_equal 2_470, DisbursementCalculation.new(@merchant).amount.cents
  end

  test '#orders_amount_sum' do
    create_completed_orders(@merchant)

    assert_equal(
      2_500,
      DisbursementCalculation.new(@merchant).orders_amount_sum.cents
    )
  end

  test 'only calculates on completed orders' do
    create_created_orders(@merchant)

    assert_equal(
      0,
      DisbursementCalculation.new(@merchant).orders_amount_sum.cents
    )
  end

  test 'only calculates on given merchant completed orders' do
    create_completed_orders(@merchant)
    create_completed_orders(@another_merchant)

    assert_equal(
      2_500,
      DisbursementCalculation.new(@merchant).orders_amount_sum.cents
    )
  end

  def create_completed_orders(merchant)
    create_list(
      :order,
      10,
      merchant: merchant,
      amount_cents: 250,
      status: 'completed'
    )
  end

  def create_created_orders(merchant)
    create_list(
      :order,
      10,
      merchant: merchant,
      amount_cents: 250,
      status: 'created'
    )
  end
end
