# frozen_string_literal: true

FactoryBot.define do
  factory :disbursement do
    merchant
    fee_cents { 1000 }
    amount_cents { 10000 }
  end
end
