# frozen_string_literal: true

FactoryBot.define do
  factory :order do
    merchant
    shopper
    disbursement
    amount_cents { 250 }
    status { 0 }
  end
end
