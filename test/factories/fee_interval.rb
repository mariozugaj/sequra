# frozen_string_literal: true

FactoryBot.define do
  factory :fee_interval do
    merchant
    from { 0.0 }
    to { nil }
    fee_rate { 0.01 }
  end
end
