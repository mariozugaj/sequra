# frozen_string_literal: true

FactoryBot.define do
  factory :merchant do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    cif { Faker::Number.number(digits: 10) }
    after(:create) { |merchant| create_fee_intervals(merchant) }
  end
end

def create_fee_intervals(merchant)
  create :fee_interval, from: 0, to: 49_99, fee_rate: 0.01, merchant: merchant
  create :fee_interval,
         from: 50_00, to: 299_99, fee_rate: 0.0095, merchant: merchant
  create :fee_interval,
         from: 300_00, to: nil, fee_rate: 0.0085, merchant: merchant
end
