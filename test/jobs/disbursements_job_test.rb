# frozen_string_literal: true

require 'test_helper'

class DisbursementsJobTest < ActiveJob::TestCase
  setup { create_list(:merchant, 10) }

  test 'enqueus DisbursementJob for each merchant' do
    assert_enqueued_jobs 10 do
      DisbursementsJob.perform_now
    end
  end
end
