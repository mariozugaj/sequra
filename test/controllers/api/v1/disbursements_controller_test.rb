# frozen_string_literal: true

require 'test_helper'

class DisbursementsControllerTest < ActionDispatch::IntegrationTest
  test 'returns a 200 if date provided' do
    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-01', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_response 200
  end

  test 'returns a 400 if no date provided' do
    get api_v1_disbursements_url,
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal({ 'date' => ['date is missing'] }, response_body)
    assert_response 400
  end

  test 'returns a 400 if no from date provided' do
    get api_v1_disbursements_url,
        params: { date: { to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal({ 'date' => { 'from' => ['from is missing'] } }, response_body)
    assert_response 400
  end

  test 'returns a 400 if no to date provided' do
    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal({ 'date' => { 'to' => ['to is missing'] } }, response_body)
    assert_response 400
  end

  test 'returns a 400 if date is in invalid format' do
    get api_v1_disbursements_url,
        params: { date: { from: '2020-2-29', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal(
      { 'date' => { 'from' => ['from is in invalid format'] } },
      response_body
    )
    assert_response 400
  end

  test 'returns a 400 if merchant is in invalid format' do
    get api_v1_disbursements_url,
        params: {
          date: { from: '2020-02-29', to: '2020-02-29' }, merchant: 'merchant'
        },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal(
      { 'merchant' => ['merchant must be an integer'] },
      response_body
    )
    assert_response 400
  end

  test 'returns max 20 results per page' do
    create_list(:disbursement, 50, created_at: Date.new(2_020, 2, 10))

    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-01', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal 20, response_body['data'].size
  end

  test 'sets the correct response headers if more than one page' do
    create_list(:disbursement, 50, created_at: Date.new(2_020, 2, 10))

    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-01', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal 20, response.headers['Page-Items'].to_i
    assert_equal 3, response.headers['Total-Pages'].to_i
    assert_equal 50, response.headers['Total-Count'].to_i
    refute_empty response.headers['Link']
  end

  test 'sets the correct response headers if less than one page' do
    create_list(:disbursement, 18, created_at: Date.new(2_020, 2, 10))

    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-01', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal 20, response.headers['Page-Items'].to_i
    assert_equal 1, response.headers['Total-Pages'].to_i
    assert_equal 18, response.headers['Total-Count'].to_i
    refute_empty response.headers['Link']
  end

  test 'returns only merchant results if merchant given' do
    merchant = create(:merchant)
    create_list(:disbursement, 10, created_at: Date.new(2_020, 2, 10))
    create_list(
      :disbursement,
      20,
      created_at: Date.new(2_020, 2, 10),
      merchant: merchant
    )

    get api_v1_disbursements_url,
        params: {
          date: { from: '2020-02-01', to: '2020-02-29' }, merchant: merchant.id
        },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_equal 20, response.headers['Page-Items'].to_i
    assert_equal 1, response.headers['Total-Pages'].to_i
    assert_equal 20, response.headers['Total-Count'].to_i
    refute_empty response.headers['Link']
  end
end
