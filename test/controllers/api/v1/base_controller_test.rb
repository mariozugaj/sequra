# frozen_string_literal: true

require 'test_helper'

class BaseControllerTest < ActionDispatch::IntegrationTest
  test 'returns a 401 if no Bearer Token provided' do
    get api_v1_disbursements_url,
        { headers: { 'ACCEPT' => 'application/json' } }

    assert_response 401
  end

  test 'returns a 200 if Bearer Token provided' do
    get api_v1_disbursements_url,
        params: { date: { from: '2020-02-01', to: '2020-02-29' } },
        headers: {
          'ACCEPT' => 'application/json',
          'Authorization' => "Bearer #{Rails.application.credentials.api_token}"
        }

    assert_response 200
  end
end
