# frozen_string_literal: true

require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest
  test 'returns a 404 if no route found' do
    get '/api/route/that/does/not/exist',
        { headers: { 'ACCEPT' => 'application/json' } }

    assert_response 404
    assert_equal 'No route matches api/route/that/does/not/exist',
                 response_body['message']
  end
end
