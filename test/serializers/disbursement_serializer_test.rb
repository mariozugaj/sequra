# frozen_string_literal: true

require 'test_helper'

class DisbursementSerializerTest < ActiveSupport::TestCase
  setup do
    @merchant = create(:merchant)
    create_list(:order, 50, status: 'completed', merchant: @merchant)
    @disbursement = DisbursementService.call(@merchant)
  end

  test 'serializing proper attributes' do
    result = DisbursementSerializer.new(@disbursement).serializable_hash
    data = result[:data]
    attributes = data[:attributes]
    relationships = data[:relationships]

    assert_equal @disbursement.id.to_s, data[:id]
    assert_equal :disbursement, data[:type]
    assert_equal @disbursement.fee.to_d, attributes[:fee]
    assert_equal @disbursement.amount.to_d, attributes[:amount]
    assert_equal @disbursement.merchant_id, attributes[:merchant_id]
    assert_equal @disbursement.created_at, attributes[:created_at]
    assert_equal @merchant.id.to_s, relationships[:merchant][:data][:id]
    assert_equal :merchant, relationships[:merchant][:data][:type]
  end
end
