# frozen_string_literal: true

require 'test_helper'

class DisbursementsQueryTest < ActiveSupport::TestCase
  test 'returns empty set if no results' do
    create_disbursements_on(Date.new(2_020, 2, 5))
    search_params = { date: { from: '2020-02-10', to: '2020-02-24' } }

    result = DisbursementsQuery.new(search_params).result

    assert_empty result
  end

  test 'returns all disbursement for given date if no merchant given' do
    create_disbursements_on(Date.new(2_020, 2, 5))
    create_disbursements_on(Date.new(2_020, 2, 12))
    create_disbursements_on(Date.new(2_020, 2, 12), create(:merchant))
    create_disbursements_on(Date.new(2_020, 2, 29))
    search_params = { date: { from: '2020-02-10', to: '2020-02-24' } }

    result = DisbursementsQuery.new(search_params).result

    refute_empty result
    assert_equal 10, result.count
  end

  test 'returns disbursement for given date for a given merchant' do
    merchant = create(:merchant)
    create_disbursements_on(Date.new(2_020, 2, 5))
    create_disbursements_on(Date.new(2_020, 2, 12))
    create_disbursements_on(Date.new(2_020, 2, 12), merchant)
    create_disbursements_on(Date.new(2_020, 2, 29))
    search_params = {
      date: { from: '2020-02-10', to: '2020-02-24' },
      merchant: merchant.id
    }

    result = DisbursementsQuery.new(search_params).result

    refute_empty result
    assert_equal 5, result.count
  end

  def create_disbursements_on(date, merchant = create(:merchant))
    create_list(:disbursement, 5, merchant: merchant, created_at: date)
  end
end
