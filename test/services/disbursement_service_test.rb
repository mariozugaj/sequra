# frozen_string_literal: true

require 'test_helper'

class DisbursementServiceTest < ActiveSupport::TestCase
  setup { @merchant = create(:merchant) }

  test 'does not create disbursement if fee and amount are zero' do
    create_list(:order, 20, merchant: @merchant, status: 'created')

    assert_no_difference -> { Disbursement.count } do
      DisbursementService.call(@merchant)
    end
  end

  test 'does not change order status if fee and amount are zero' do
    create_list(:order, 20, merchant: @merchant, status: 'created')

    assert_no_difference -> { Order.disbursed.count } do
      DisbursementService.call(@merchant)
    end
  end

  test 'creates Disbursement for given merchant' do
    create_list(:order, 20, merchant: @merchant, status: 'completed')

    assert_difference -> { Disbursement.count }, 1 do
      DisbursementService.call(@merchant)
    end
  end

  test 'changes order status to disbursed for all completed orders' do
    create_list(:order, 20, merchant: @merchant, status: 'completed')

    assert_difference -> { Order.disbursed.count }, 20 do
      DisbursementService.call(@merchant)
    end
  end

  test 'links orders with created Disbursement' do
    create_list(
      :order,
      20,
      merchant: @merchant,
      status: 'completed',
      disbursement: nil
    )
    assert_equal [nil], Order.pluck(:disbursement_id).uniq

    DisbursementService.call(@merchant)

    assert_equal [Disbursement.last.id], Order.pluck(:disbursement_id).uniq
  end
end
