# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api, constraints: { format: :json } do
    namespace :v1 do
      resources :disbursements, only: :index
    end
  end
  get '*unmatched_route', to: 'application#not_found'
end
