# frozen_string_literal: true

every :monday, at: '12pm' do
  DisbursementsJob.perform_later
end
